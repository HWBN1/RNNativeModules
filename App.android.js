import React from "react";
import {
  StyleSheet,
  Button,
  View,
  NativeModules,
  Text,
  Image,
  ScrollView
} from "react-native";

const { Instacapture } = NativeModules;

export default class App extends React.Component {
  state = {
    image: null
  };

  openInstacapture = () => {
    Instacapture.open();
  };

  async capture() {
    try {
      const png = await Instacapture.capture();

      this.setState({ image: png });
    } catch (error) {
      alert("Cannot take screenshot! Something went wrong!");
      console.warn(error);
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.h1}>Welcome to Instacapture!</Text>
        </View>
        <View style={styles.section}>
          <Button
            style={{ padding: 10, flex: 1 }}
            color="#445060"
            onPress={() => this.openInstacapture()}
            title="Open Instacapture"
          />
        </View>
        <View style={styles.section}>
          <Button
            style={{ padding: 10, flex: 1 }}
            color="#445060"
            onPress={() => this.capture()}
            title="Take Screenshot"
          />
        </View>
        {this.state.image && (
          <View style={{ padding: 10, flex: 3 }}>
            <Text style={styles.imageText}>Your image</Text>
            <Image
              source={{ uri: `data:image/png;base64,${this.state.image}` }}
              style={styles.image}
              resizeMode="contain"
            />
          </View>
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    color: "#f9f9f9"
  },
  section: {
    padding: 10,
    flex: 1
  },
  headerContainer: {
    height: 100,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20
  },
  h1: {
    flex: 1,
    fontSize: 32
  },
  openButton: {
    padding: 10
  },
  imageText: {
    fontSize: 14,
    color: "black",
    marginTop: 20,
    marginBottom: 10
  },
  image: {
    width: "100%",
    height: 500,
    borderColor: "#293821",
    borderWidth: 1,
    marginBottom: 20
  }
});

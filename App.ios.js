import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    NativeModules
} from 'react-native';

const { RNIBGxNetworkManager } = NativeModules;

export default class App extends React.Component {
    state = {
        loading: true,
        list: null,
        error: false
    }

    componentWillMount() {
        this.getRequest();
    }

    getRequest = () => {
        RNIBGxNetworkManager.get(
            'http://www.pureexample.com/backend/data/car-sale.json',
            (error, res) => {
                if (error) {
                    alert(error);
                    this.setState({ loading: false, error: true });
                } else {
                    this.setState({ loading: false, error: false, list: res });
                }
            }
        );
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <View style={styles.container}>

                <View style={styles.headerContainer}>
                    <Text style={styles.h1}>Cars sale report</Text>
                    <Text style={styles.h3}>Month 2012-11</Text>
                </View>
                {!this.state.error && (
                    <ScrollView style={styles.listContainer}>
                        <View style={styles.itemContainer}>
                            <View style={styles.itemTitleContainer}>
                                <Text style={{ fontWeight: 'bold' }}> Manufacturer </Text>
                                <Text style={{ fontWeight: 'bold' }}> Sold  </Text>
                            </View>
                            <View style={[styles.separator, { height: 2 }]} />

                        </View>
                        {this.state.list.map((item, i) => (
                            <View key={i} style={styles.itemContainer}>
                                <View key={i} style={styles.itemTitleContainer}>
                                    <Text> {item.Manufacturer} </Text>
                                    <Text> {item.Sold} </Text>
                                </View>
                                <View style={styles.separator} />
                            </View>
                        ))}
                    </ScrollView>
                )}
                {this.state.error && (
                    <View style={styles.errorContainer}>
                        <Text>{this.state.error}</Text>
                        <TouchableOpacity onPress={() => this.getRequest()}>
                            <Text>Try Again!</Text>
                        </TouchableOpacity>
                    </View>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        color: "#f9f9f9"
    },
    headerContainer: {
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 20
    },
    h1: {
        flex: 1,
        fontSize: 32,
         color: "#445060"
    },
    h3: {
        flex: 1,
        fontSize: 24,
        color: '#44506090',
    },
    listContainer: {
        marginTop: 10
    },
    itemContainer: {
        padding: 20
    },
    itemTitleContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    separator: {
        height: 1,
        width: '100%',
        backgroundColor: '#445060',
        marginTop: 20
    },
    loadingContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    errorContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
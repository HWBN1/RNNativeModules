# RNNativeModules

https://github.com/Instabug/cross-platform-task/blob/master/task.md

## Installation

```
git clone https://gitlab.com/HWBN1/RNNativeModules.git
cd RNNativeModules
npm install
react-native run-android/ios
```


## Usage

#### Android

[Instacapture](https://github.com/Instabug/cross-platform-task/raw/master/Instacapture.zip): used to capture a screenshot of all the current content on a screen.

Import from NativeModules

```
import { NativeModules } from 'react-native';
const { Instacapture } = NativeModules;
```

You can capture a screenshot

```
 async capture() {
    try {
      const png = await Instacapture.capture();

      this.setState({ image: png });
    } catch (error) {
      alert("Cannot take screenshot! Something went wrong!");
    }
  }
  
   renderImage = () => (
    <Image source={{ uri: `data:image/png;base64,${this.state.image}` }}/>
  );
```

You can take a look to the native module

```
 Instacapture.open();
```

#### IOS

[IBGxNetworkManager](https://github.com/Instabug/cross-platform-task/raw/master/IBGxNetworkManager.zip): used to create and run HTTP requests conveniently.

Import from NativeModules 

```
import { NativeModules } from 'react-native';
const { RNIBGxNetworkManager } = NativeModules;
```

You can get a request via IBGxNetworkManager

```
  RNIBGxNetworkManager.get(
      "http://www.pureexample.com/backend/data/car-sale.json",
      (error, res) => {
        if (error) {
          console.log("error:", error); // Print the error
        } else if (res) {
          console.log("result:", res); // Print the result
        }
      }
    );
```

## Reference

[Native Modules IOS ](https://facebook.github.io/react-native/docs/native-modules-ios)

[Native Modules Android](https://facebook.github.io/react-native/docs/native-modules-android)

https://github.com/salmamali/SampleApp

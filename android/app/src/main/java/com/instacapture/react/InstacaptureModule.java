package com.instacapture.react;

import android.content.Intent;
import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Base64;
import android.widget.Toast;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.HashMap;
import java.util.Map;
import java.io.ByteArrayOutputStream;

import com.tarek360.instacapture.Instacapture;
import com.tarek360.instacapture.listener.SimpleScreenCapturingListener;
import com.tarek360.sample.MainActivity;

public class InstacaptureModule extends ReactContextBaseJavaModule {

    public InstacaptureModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "Instacapture";
    }

    @ReactMethod
    public void open() {
        ReactApplicationContext context = getReactApplicationContext();
        if (context != null) {
            Intent rctActivityIntent = new Intent(context, MainActivity.class);
            context.startActivity(rctActivityIntent);
        }
    }

    @ReactMethod
    public void capture(final Promise onCaptureComplete) {
        Activity activity = getCurrentActivity();

        if (activity == null) {
            return;
        }

        Instacapture.capture(activity, new SimpleScreenCapturingListener() {
            @Override
            public void onCaptureComplete(Bitmap bitmap) {
                super.onCaptureComplete(bitmap);
                onCaptureComplete.resolve(encodeToBase64(bitmap));

            }

            @Override
            public void onCaptureFailed(Throwable e) {
                super.onCaptureFailed(e);
                onCaptureComplete.reject(e);
            }
        }, null);
    }

    private static String encodeToBase64(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }
}

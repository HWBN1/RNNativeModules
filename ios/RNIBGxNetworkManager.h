//
//  RNIBGxNetworkManager.h
//  RNNativeModules
//
//  Created by Mohamed Hamed. on 11/17/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#ifndef RNIBGxNetworkManager_h
#define RNIBGxNetworkManager_h


#endif /* RNIBGxNetworkManager_h */

#import <React/RCTBridgeModule.h>
#import <IBGxNetworkManager/IBGxNetworkManager.h>

@interface RNIBGxNetworkManager : IBGxNetworkManager <RCTBridgeModule>

@end

